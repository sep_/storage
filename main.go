package main

import (
	"fmt"
	"math/rand"
	"storage/faststorage"
	"storage/storage"
	"sync"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

func randomKey(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[rand.Intn(len(charset))]
	}
	return string(b)
}

func benchmarkStorage() {
	start := time.Now()
	wg := sync.WaitGroup{}
	storage := storage.NewStorage()
	for i := 0; i < 1000000; i++ {
		go func() {
			wg.Add(1)
			key := randomKey(5)
			storage.Set(key, [5000]byte{})
			wg.Done()
		}()
	}
	wg.Wait()
	elapsed := time.Since(start)
	fmt.Printf("Storage set took %s\n", elapsed)
}

func benchmarkFastStorage(storage *faststorage.FastStorage) {
	start := time.Now()
	wg := sync.WaitGroup{}
	for i := 0; i < 1000000; i++ {
		go func() {
			wg.Add(1)
			key := randomKey(5)
			storage.FastSet(key, [5000]byte{})
			wg.Done()
		}()
	}
	wg.Wait()
	elapsed := time.Since(start)
	fmt.Printf("FastStorage set took %s\n", elapsed)
}

func main() {
	storage := faststorage.NewFastStorage()
	benchmarkFastStorage(storage)
	benchmarkStorage()
}
