package storage

import (
	"errors"
	"sync"
)

//Storage is like a Go map but is safe for concurrent use
//by multiple goroutines without additional locking or coordination.
type Storage struct {
	storage map[string]interface{}
	lock    sync.RWMutex
}

//NewStorage return ready to use Storage
func NewStorage() *Storage {
	return &Storage{
		storage: make(map[string]interface{}),
		lock:    sync.RWMutex{},
	}
}

//Set Loads the key value pair in the storage.
func (s *Storage) Set(key string, value interface{}) {
	s.lock.Lock()
	s.storage[key] = value
	s.lock.Unlock()
}

//Get Recieve the value coresponding to key in the storage.
//If fail it return "value not found"
func (s *Storage) Get(key string) (interface{}, error) {
	s.lock.RLock()
	value, exist := s.storage[key]
	if exist == false {
		return nil, errors.New("value not found")
	}
	s.lock.RUnlock()
	return value, nil
}
