package storage

import (
	"errors"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSet(t *testing.T) {
	storage := NewStorage()
	storage.Set("test", 10)
	value, exist := storage.storage["test"]
	assert.Equal(t, exist, true)
	assert.Equal(t, value, 10)
}

func TestGet(t *testing.T) {
	testStorage := make(map[string]interface{})
	testStorage["test"] = 10
	storage := Storage{storage: testStorage, lock: sync.RWMutex{}}
	value, err := storage.Get("test")
	assert.Equal(t, err, nil)
	assert.Equal(t, value, 10)
}

func TestGetNotFound(t *testing.T) {
	storage := Storage{storage: make(map[string]interface{}), lock: sync.RWMutex{}}
	value, err := storage.Get("test")
	assert.Equal(t, err, errors.New("value not found"))
	assert.Equal(t, value, nil)
}
