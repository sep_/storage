PROJECT_NAME := "storage"
PKG := "storage"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
GOPATH := ${HOME}/go

.PHONY: all dep build clean test coverage coverhtml lint unit-test

all: build

lint-test:
	@golint -set_exit_status ${PKG_LIST}

vet-test:
	@go vet ${PKG_LIST}

fmt-test:
	@export unformatted=$$(gofmt -l ${GO_FILES} ) ;\
	if [ ! -z $$unformatted ] ; then echo $$unformatted ; exit 1; fi ;\
	exit 0 ;

fmt:
	@go fmt ./...

test: unit-test race-test

unit-test: export EXEC_MODE = TEST
unit-test: dep  
	@go test -count=1 -short ${PKG_LIST}

race-test: export EXEC_MODE = TEST
race-test: dep  
	@go test -race -count=1 -short ${PKG_LIST}

dep: 
	@go mod download; \
	go mod verify;

clean: 
	@rm -f $(PROJECT_NAME)

run: build
	./api

# msan: dep
# 	@go test -msan -short ${PKG_LIST}

# coverage:
# 	./tools/coverage.sh;

# coverhtml: 
# 	./tools/coverage.sh html;
