package faststorage

import (
	"errors"
	"sync"
)

//FastStorage is like a Go map but is safe for concurrent use
//by multiple goroutines without additional locking or coordination.
type FastStorage struct {
	nodes    map[rune]*FastStorage
	value    interface{}
	nodeLock *sync.RWMutex
	setLock  *sync.Mutex
}

//NewFastStorage create ready to use fastStorage.
func NewFastStorage() *FastStorage {
	return &FastStorage{
		nodes:    make(map[rune]*FastStorage),
		nodeLock: &sync.RWMutex{},
		setLock:  &sync.Mutex{},
	}
}

//FastSet Loads the key value pair in the storage.
func (s *FastStorage) FastSet(key string, value interface{}) {
	var storage *FastStorage
	storage = s
	for _, ch := range key {
		storage.nodeLock.RLock()
		nextStorage, exist := storage.nodes[ch]
		storage.nodeLock.RUnlock()
		if exist == false {
			nextStorage = NewFastStorage()
			storage.nodeLock.Lock()
			storage.nodes[ch] = nextStorage
			storage.nodeLock.Unlock()
		}
		storage = nextStorage
	}
	storage.setLock.Lock()
	storage.value = value
	storage.setLock.Unlock()
}

//FastGet Recieve the value coresponding to key in the storage.
//If fail it return "value not found"
func (s *FastStorage) FastGet(key string) (interface{}, error) {
	var storage *FastStorage
	storage = s
	for _, ch := range key {
		nextStorage, exist := storage.nodes[ch]
		if exist == false {
			return nil, errors.New("value not found")
		}
		storage = nextStorage
	}
	return storage.value, nil
}
