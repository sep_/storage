package faststorage

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFastStoragetSet(t *testing.T) {
	storage := NewFastStorage()
	storage.FastSet("ab", 10)
	firstLayer, _ := storage.nodes['a']
	assert.Equal(t, firstLayer.nodes['b'].value, 10)
}

func TestFastStoragetGet(t *testing.T) {
	root := NewFastStorage()
	nodeA := NewFastStorage()
	nodeB := NewFastStorage()
	root.nodes['a'] = nodeA
	nodeA.nodes['b'] = nodeB
	nodeB.value = 10
	testStorage := root
	value, err := testStorage.FastGet("ab")
	assert.Equal(t, value, 10)
	assert.Equal(t, err, nil)
}

func TestFastStoragetGetNotFound(t *testing.T) {
	root := NewFastStorage()
	nodeA := NewFastStorage()
	nodeB := NewFastStorage()
	root.nodes['a'] = nodeA
	nodeA.nodes['b'] = nodeB
	nodeB.value = 10
	testStorage := root
	value, err := testStorage.FastGet("abc")
	assert.Equal(t, value, nil)
	assert.Equal(t, err, errors.New("value not found"))
}
