# Storage

Storage is optimized thread safe key-value package written in golang.

## Benchmark

Benchmark for 1 Milion insert(each insert 5 KB of data).[bencmark code main.go]

FastStorage set took 2.873800222s.

Storage set took 27.113913176s.

## General Idea

The perfomance gain of fast-storage is because of not locking the whole storage in each insert instead
it uses trie data structure to only lock the required one.

## Further Optimization

The trie implementation is not optimized for space we can change it to radix tree to save some memory.
